var pictureArray = ['aap','hond','ijsbeer','kat','leeuw','penguin','uil','vlinder','wolf'];
var correctPicture = randomPic();

window.addEventListener("load", init);

function init(){
    pictureGrid();
    showRandomPic();
}

function pictureGrid(){
    for(var index in pictureArray){
        var img = document.createElement('img');
        img.setAttribute('src', './images/grid/'+pictureArray[index]+'.jpg');
        img.setAttribute('class', 'img--small');
        img.setAttribute('id', pictureArray[index]);
        img.addEventListener('click', handleImgClickEvent);
        var ph = document.getElementById('grid');
        ph.appendChild(img);
    }
}

function handleImgClickEvent(){
    console.log(event.target.id);
    var chosenPicture = event.target.id;
    if (chosenPicture==correctPicture){
        document.getElementById('message').innerHTML = 'Je hebt het goed geraden!';
        correctPicture = randomPic();
        showRandomPic();
    }else{
        document.getElementById('message').innerHTML = 'Probeer het nog een keer.';
    }
}

function showRandomPic(){
    var img = document.getElementById('randomimg');
    img.setAttribute('src', './images/grid/'+correctPicture+'.jpg');
}

function randomPic(){
    return pictureArray[Math.floor(pictureArray.length * Math.random())];
}